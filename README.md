The project code is adapted from https://github.com/EliasGit2017/derk3-MARL

Environment
===========

Install dependencies and set up the development environment with
[Conda](https://docs.conda.io/en/latest/).

```sh
conda create -n cs234_fp
conda activate cs234_fp
pip install -r requirements.txt
```

First make sure the Derk's Gym environment runs by following the getting started
[instructions](https://gym.derkgame.com/).

Training
========

The training script will train an agent with the given hyperparameters using
self-play and output a checkpoint file.

```sh
python train.py --episodes 300 --checkpoint checkpoint.pt parameters.json
```

Test
========
To see how the model performs, run the below command to run the agent against random opponent
```sh
python run.py --random
```