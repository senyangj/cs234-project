from abc import ABC, abstractmethod

import torch
from torch import Tensor
from torch.distributions import Normal, Categorical

class MultiDiscreteDistribution:
    movex_dist: Categorical
    rotate_dist: Categorical
    chasefocus_dist: Categorical
    castslot_dist: Categorical
    focus_dist: Categorical

    def __init__(
        self,
        movex: Categorical,
        rotate: Categorical,
        chasefocus: Categorical,
        castslot: Categorical,
        focus: Categorical,
    ) -> None:
        self.movex_dist = movex
        self.rotate_dist = rotate
        self.chasefocus_dist = chasefocus
        self.castslot_dist = castslot
        self.focus_dist = focus

    def sample(self, deterministic: bool = False):
        if deterministic:
            movex = self.movex_dist.probs.argmax(-1)
            rotate = self.rotate_dist.probs.argmax(-1)
            chasefocus = self.chasefocus_dist.probs.argmax(-1)
            castslot = self.castslot_dist.probs.argmax(-1)
            focus = self.focus_dist.probs.argmax(-1)
        else:
            movex = self.movex_dist.sample()
            rotate = self.rotate_dist.sample()
            chasefocus = self.chasefocus_dist.sample()
            castslot = self.castslot_dist.sample()
            focus = self.focus_dist.sample()
        return movex, rotate, chasefocus, castslot, focus

    def mean_entropy(self):
        joint_independent_entropy = (
            self.movex_dist.entropy()
            + self.rotate_dist.entropy()
            + self.chasefocus_dist.entropy()
            + self.castslot_dist.entropy()
            + self.focus_dist.entropy()
        )
        return torch.mean(joint_independent_entropy / 5)

    def log_likelihood(
        self, movex: Tensor, rotate: Tensor, chasefocus: Tensor, castslot: Tensor, focus: Tensor
    ):
        return torch.stack(
            [
                self.movex_dist.log_prob(movex),
                self.rotate_dist.log_prob(rotate),
                self.chasefocus_dist.log_prob(chasefocus),
                self.castslot_dist.log_prob(castslot),
                self.focus_dist.log_prob(focus),
            ],
            dim=-1,
        )
