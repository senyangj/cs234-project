from typing import Tuple, NamedTuple

import numpy as np
import torch
from torch.autograd import Function
from torch.nn import (
    Linear,
    Module,
    ReLU,
    Sequential,
    ModuleList,
)
from torch import FloatTensor, Tensor
from torch.distributions import Normal, Categorical

from .distribution import MultiDiscreteDistribution

class RNDModel(Module):
    def __init__(
            self,
            observation_size: int = 64,
            hidden_size: Tuple[int, int, int] = (512, 256, 256),
        ):

        super().__init__()

        # Prediction network
        self.predictor = Sequential(
            Linear(observation_size, hidden_size[0]),
            ReLU(),
            Linear(hidden_size[0], hidden_size[1]),
            ReLU(),
            Linear(hidden_size[1], hidden_size[2]),
        )

        # Target network
        self.target = Sequential(
            Linear(observation_size, hidden_size[0]),
            ReLU(),
            Linear(hidden_size[0], hidden_size[1]),
            ReLU(),
            Linear(hidden_size[1], hidden_size[2]),
        )

        # target network is not trainable
        for param in self.target.parameters():
            param.requires_grad = False

    def forward(self, obs):
        predict_feature = self.predictor(obs)
        with torch.no_grad():
            target_feature = self.target(obs)
        
        return predict_feature, target_feature
    
    def compute_intrinsic_reward(self, obs: np.ndarray):
        if torch.cuda.is_available():
            device_type = "cuda"
        elif torch.backends.mps.is_available():
            device_type = "mps"
        else:
            device_type = "cpu"
        obs = torch.as_tensor(
            obs,
            dtype=torch.float,
            device=device_type
        )
        predict_feature, target_feature = self.forward(obs)
        out = torch.linalg.vector_norm(predict_feature - target_feature, dim=-1)**2
        return out.cpu().detach().numpy()


class MultiDiscretePolicyModel(Module):
    def __init__(
        self,
        observation_size: int = 64,
        action_size: Tuple[int, int, int, int, int] = (9, 9, 9, 4, 8),
        hidden_size: Tuple[int, int, int] = (512, 256, 256),
    ):
        super().__init__()

        self.action_size = action_size

        # map observations to common embeddings
        self.shared = Sequential(
            Linear(observation_size, hidden_size[0]),
            ReLU(),
            Linear(hidden_size[0], hidden_size[1]),
            ReLU(),
        )

        # map common embeddings to action heads
        self.policy = ModuleList(
            Sequential(
                Linear(hidden_size[1], hidden_size[2]),
                ReLU(),
                Linear(hidden_size[2], size),
                # LogSoftmax(-1),
            )
            for size in action_size
        )

    def forward(self, observation: FloatTensor) -> MultiDiscreteDistribution:
        shared = self.shared(observation)
        return MultiDiscreteDistribution(
            *(Categorical(logits=policy(shared)) for policy in self.policy)
        )


class IndependentValueModel(Module):
    def __init__(
        self,
        observation_size: int = 64,
        hidden_size: Tuple[int, int, int] = (512, 512, 256),
    ):
        super().__init__()

        self.observation_size = observation_size
        self.hidden_size = hidden_size

        self.value = Sequential(
            Linear(self.observation_size, self.hidden_size[0]),
            ReLU(),
            Linear(self.hidden_size[0], self.hidden_size[1]),
            ReLU(),
            Linear(self.hidden_size[1], self.hidden_size[2]),
            ReLU(),
            Linear(self.hidden_size[2], 1),
        )

    def forward(self, observation: FloatTensor) -> FloatTensor:
        return self.value(observation)


class CentralizedValueModel(Module):
    def __init__(
        self,
        observation_size: int = 64,
        hidden_size: Tuple[int, int, int] = (512, 512, 256),
    ):
        super().__init__()

        self.observation_size = observation_size
        self.hidden_size = hidden_size

        self.value = Sequential(
            Linear(3 * self.observation_size, self.hidden_size[0]),
            ReLU(),
            Linear(self.hidden_size[0], self.hidden_size[1]),
            ReLU(),
            Linear(self.hidden_size[1], self.hidden_size[2]),
            ReLU(),
            Linear(self.hidden_size[2], 1),
        )

    def forward(self, observation: FloatTensor) -> FloatTensor:
        # combine observation for 3 agents
        observation = observation.flatten(start_dim=-2)
        value =  self.value(observation)
        return value
